var cursorAdapterAutoComplete = require('ancient-cursor').adapterAutoComplete;
var lodash = require('lodash');

module.exports = function adapterConstructor() {
  var counter = 1;
  var atomicsStorage = {};
  
  return function adapterConstructor() {
    
    // (id: String, callback: (error?, result?))
    this.getRef = function(id, callback) {
      callback(undefined, atomicsStorage[id]);
    };
    
    // (data: any) => id: String
    this.newRef = function(data) {
      return data.id;
    };
    
    // (id: String, callback: (error?, result?: Boolean))
    this.deleteRef = function(adapter, id, callback) {
      if (atomicsStorage[id]) {
        delete atomicsStorage[id];
        callback(undefined, true);
      } else {
        callback(undefined, undefined);
      }
    };
    
    // (data: any, options: any, callback: (error?, id?: String))
    this.createAtomic = function(data, options, callback) {
      data.id = ""+counter++;
      atomicsStorage[data.id] = data;
      callback(undefined, data.id);
    };
    
    // (selector: any, modifier: any, options: any, callback: (error?, count?: Number))
    this.updateAtomic = function(selector, modifier, options, callback) {
      var resultCount = 0;
      if (typeof(selector) == 'string') {
        if (atomicsStorage[selector]) {
          atomicsStorage[selector] = modifier(atomicsStorage[selector]);
          resultCount++;
        }
      } else {
        for (var id in atomicsStorage) {
          if (lodash.isMatch(atomicsStorage[id], selector)) {
            atomicsStorage[id] = modifier(atomicsStorage[id]);
            resultCount++;
          }
        }
      }
      callback(undefined, resultCount);
    };
    
    // (selector: any, callback: (error?, count?: Number))
    this.deleteAtomic = function(selector, callback) {
      var resultCount = 0;
      if (typeof(selector) == 'string') {
        if (atomicsStorage[selector]) {
          delete atomicsStorage[selector];
          resultCount++;
        }
      } else {
        for (var id in atomicsStorage) {
          if (lodash.isMatch(atomicsStorage[id], selector)) {
            delete atomicsStorage[id];
            resultCount++;
          }
        }
      }
      callback(undefined, resultCount);
    };
    
    // (selector: any, options: any) => cursorPointer: any
    this.searchAtomic = function(selector, options) {
      var _selector = (typeof(selector) == 'string')?{ id: selector }:selector;
      return { selector: _selector, options, options };
    };
    
    // (pointer: any, callback: (error?, results))
    this.cursorFetch = function(pointer, callback) {
      var results = [];
      for (var id in atomicsStorage) {
        if (lodash.isMatch(atomicsStorage[id], pointer.selector)) results.push(atomicsStorage[id]);
      }
      callback(undefined, results);
    };
    
    // (pointer: any, handler: (error?, result), callback: ())
    this.cursorEach = function(pointer, handler, callback) {
      for (var id in atomicsStorage) {
        if (lodash.isMatch(atomicsStorage[id], pointer.selector)) handler(undefined, atomicsStorage[id]);
      }
      callback(undefined);
    };
    
    cursorAdapterAutoComplete(this);
    
    return this;
  };
};