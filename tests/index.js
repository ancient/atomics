var assert = require('chai').assert;
var lodash = require('lodash');
var Adapters = require('ancient-adapters').Adapters;
var Refs = require('ancient-refs');
var Atomics = require('../index.js').Atomics;
var Cursor = require('ancient-cursor');

var testCursorAdapter = require('ancient-cursor/tests/test.js');

var DefaultObjectAdapter = require('./defaultAdapter.js');
var DocumentsFactory = require('ancient-document').Factory;

describe('ancient/atomics', function() {
  var adapters = new Adapters();
  var documentsFactory = new DocumentsFactory(adapters);
  var refs = new Refs(documentsFactory);
  adapters.addClass('atomics', Atomics.adaptersAddClass);
  
  describe('should check atomics only on defaultAdapter', function() {
    adapters.add('posts', DefaultObjectAdapter());
    var posts;
    it('should construct', function() {
      posts = adapters.get('posts').asClass('atomics');
    });
    var id;
    it('should create', function() {
      id = posts.create({ abc: 1 });
      assert.isString(id);
      var post = adapters.refs.get('posts/'+id);
      assert.isObject(post);
      assert.equal(post.data.abc, 1);
    });
    var cursor;
    it('should search', function() {
      cursor = posts.search(id);
      assert.equal(cursor.count(), 1);
      cursor = posts.search({ abc: 1 });
      assert.equal(cursor.count(), 1);
    });
    it('should update', function() {
      var updated = posts.update({ abc: 1 }, function(data) { data.abc = 2; return data; });
      assert.equal(updated, 1);
      var post = adapters.refs.get('posts/'+id);
      assert.isObject(post);
      assert.equal(post.data.abc, 2);
      var updated = posts.update(id, function(data) { data.abc = 3; return data; });
      assert.equal(updated, 1);
      var post = adapters.refs.get('posts/'+id);
      assert.isObject(post);
      assert.equal(post.data.abc, 3);
    });
    it('should delete', function() {
      var deleted = posts.delete({ abc: 1 });
      assert.equal(deleted, 0);
      var deleted = posts.delete({ abc: 3 });
      assert.equal(deleted, 1);
    });
  });
  
  describe('test demo atomicsStorage Cursor adapter', function() {
    adapters.add('cursorTestAdapter', DefaultObjectAdapter());
    var atomics = new Atomics(refs, 'cursorTestAdapter');
    var a = atomics.create({ abc: false });
    var b = atomics.create({ abc: true });
    var c = atomics.create({ abc: true });
    testCursorAdapter(atomics.search({ abc: true }), [
      { id: b, abc: true },
      { id: c, abc: true }
    ], true, true);
  });
});