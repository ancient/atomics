# Atomics^0.0.0 ^[wiki](https://gitlab.com/ancient/atomics/wikis/home)

Universal operation on atomic entities in adapters.

###### `object` adapters

It is a object with user adapted methods of any of databases for some class.

A detailed description of the requirements to the adapter can be found in the [wiki](https://gitlab.com/ancient/atomics/wikis/home).

[Adapters repository](https://gitlab.com/ancient/adapters).

###### `Document` support

Returns links wrapped in a [Document](https://gitlab.com/ancient/document) capsule.

```js
document.data; // { id: String, source?: Ref, target?: Ref }
document.Ref() // "someAdapter/uniqueForCurrentStorageStringId"
```

###### `Cursor` support

Search wrapped in a [Cursor](https://gitlab.com/ancient/cursor) class.

###### Example

```js
var Refs = require('ancient-refs');
var DocumentsFactory = require('ancient-document').Factory;
var Atomics = require('ancient-atomics');
var Adapters = require('ancient-adapters').Adapters;
var DefaultObjectAdapter = require('ancient-atomics/defaultObjectAdapter');

var adapters = new Adapters();
adapters.add('posts', DefaultObjectAdapter());

var documentsFactory = new DocumentsFactory(adapters);
var refs = new Refs(documentsFactory);

var posts = new Atomics(refs, adapters, 'posts');

posts.create({ body: 'Hello World!' });
// "1"

posts.create({ body: 'My first record in blog.' });
// "2"

posts.update(postId, { body: 'Goodbye World!' });
// 1

var cursor = posts.search("1");
cursor.fetch();
// [ Document { data: { id: "1", body: 'Goodbye World!' } } ]

posts.searchOne({ body: 'Goodbye World!' });
// Document { data: { id: "1", body: 'Goodbye World!' } }

posts.update({}, { body: 'Updated post.' });

var cursor = posts.search({});
cursor.fetch();
/** [
Document { data: { id: "1", body: 'Updated post.' } }
Document { data: { id: "2", body: 'Updated post.' } }
] **/

posts.delete({ body: 'Updated post' });
// 2

var cursor = posts.search({});
cursor.count();
0
```

## Versions