var Cursor = require('ancient-cursor').Cursor;

/** Atomics **/
// new (refs: Refs, adapterName: String)
var Atomics = function(refs, adapterName) {
  this.refs = refs;
  this.adapters = refs.adapters;
  this.adapterName = adapterName;
  this.adapter = this.adapters.get(this.adapterName);
};

// (data: any, options?: any, callback: (error?, id?: String)) => id?: String
Atomics.prototype.create = function(data, options, callback) {
  var result;
  this.adapter.createAtomic(data, options, function(error, _result) {
    result = _result;
    if (callback) callback(error, result);
  });
  return result;
};

// (selector: any, modifier: any, options?: any, callback?: (error?, count?: Number)) => count?: Number
Atomics.prototype.update = function(selector, modifier, options, callback) {
  var result;
  this.adapter.updateAtomic(selector, modifier, options, function(error, _result) {
    result = _result;
    if (callback) callback(error, result);
  });
  return result;
};

// (selector: any, options?: any, callback?: (error?, cursor?: Cursor)) => cursor?: Cursor
Atomics.prototype.search = function(selector, options, callback) {
  var pointer = this.adapter.searchAtomic(selector, options);
  var cursor = new Cursor(this.refs, this.adapterName, pointer);
  if (callback) callback(undefined, cursor);
  return cursor;
};

// (selector: any, callback: (error?, count?: Number)) => count?: Number
Atomics.prototype.delete = function(selector, callback) {
  var result;
  this.adapter.deleteAtomic(selector, function(error, _result) {
    result = _result;
    if (callback) callback(error, result);
  });
  return result;
};

// (adapter: Adapter) => Function
Atomics.adaptersAddClass = function(adapter) {
  return new Atomics(adapter.adapters.refs, adapter.adapterName);
};

exports.Atomics = Atomics